Source: django-jinja
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Edward Betts <edward@4angle.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               python3-all,
               python3-setuptools
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/niwibe/django-jinja
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-jinja
Vcs-Git: https://salsa.debian.org/python-team/packages/django-jinja.git

Package: python3-django-jinja
Architecture: all
Depends: python3-django, ${misc:Depends}, ${python3:Depends}
Description: Jinja2 templating language integrated in Django (Python 3 version)
 Jinja2 provides certain advantages over the native system of Django, for
 example, explicit calls to callable from templates, has better performance
 and has a plugin system
 .
 Features:
 .
    * Auto-load templatetags compatible with Jinja2 the same way as Django.
    * Django templates can coexist with Jinja2 templates without any problems.
      It works as middleware, intercepts Jinja templates by file path pattern.
    * Django template filters and tags can mostly be used in Jinja2 templates.
    * I18n subsystem adapted for Jinja2 (makemessages now collects messages
      from Jinja templates)
    * jinja2 bytecode cache adapted for using django's cache subsystem.
    * Support for django context processors.
